 # Autor: David Spilka (xspilk00)
# Program: FTP klient - 1. projekt IPK

NAME = ftpclient
CC = g++
CFLAGS = -Wall -g -pedantic -Wno-unused-variable -Wno-unused-but-set-variable

$(NAME): $(NAME).cpp
	$(CC) $(CFLAGS) $(NAME).cpp -o $(NAME) 
