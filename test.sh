#!/bin/bash

rm test

for i in {1..50}
do
	for server in "ftp://ftp.fit.vutbr.cz" "ftp://anonymous:secret@ftp.fit.vutbr.cz:21/pub/systems/centos" "ftp.linux.cz/pub/local/" "ftp.mgo.opava.cz" "ftp.altap.cz" "ftp://gaia.cs.umass.edu/"
	do
		echo ""
	   	echo "---------------------------------------------------------------"
		echo ""
	   	echo $i . " - " . $server
	   	./ftpclient $server 2>> test
	done
done
