/* 
Autor: David Spilka (xspilk00)
Program: FTP klient - 1. projekt IPK
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>

#include <string>
#include <iostream>
#include <sstream>

#define MAX_SIZE 30000

using namespace std;

char * user;
char * pass;
char * host;
char * port;
char * path;

bool is_set_user = false;
bool is_set_pass = false;
bool is_set_host = false;
bool is_set_port = false;
bool is_set_path = false;

enum { 
	NO_ERROR = 0,
	ERR_BADPARAMS,
	ERR_SOCKET,
	ERR_GETHOSTBYNAME,
	ERR_CONNECT,
	ERR_READ,
	ERR_NON220,
	ERR_NON331,
	ERR_NON230,
	ERR_NON227,
	ERR_NON250,
	ERR_NON150,
	ERR_NON226,
	ERR_WRITE
};

const char *errors[] = {
	"Zadna chyba",
	"Chybně zadaný parametr. Spustě program pomocí ./ftpclient [ftp://[user:password@]]host[:port][/path][/]",
	"Chyba vytvoření socketu",
	"Chyba, dle jména nenalezen server",
	"Chyba připojení k serveru",
	"Chyba při čtení odpovědi od serveru",
	"Chyba: špatná odpověď serveru, očekáván kód 220",
	"Chyba: špatná odpověď serveru, očekáván kód 331",
	"Chyba: špatná odpověď serveru, očekáván kód 230",
	"Chyba: špatná odpověď serveru, očekáván kód 227",
	"Chyba: špatná odpověď serveru, očekáván kód 250",
	"Chyba: špatná odpověď serveru, očekáván kód 150",
	"Chyba: špatná odpověď serveru, očekáván kód 226",
	"Chyba při zasílání zprávy serveru",
};

void printErr(int error_code);
void freeAll();
void solveParams(int argc, char ** argv);
