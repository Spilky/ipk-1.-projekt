/* 
Autor: David Spilka (xspilk00)
Program: FTP klient - 1. projekt IPK
*/

#include "ftpclient.h"

int main(int argc, char ** argv) {
	
	solveParams(argc, argv);

	char msg[MAX_SIZE];
	string line;

	struct sockaddr_in sin;
	struct hostent *hptr;
	int s, n;

	sin.sin_family = PF_INET;
	sin.sin_port = (is_set_port ? htons(atoi(port)) : htons(21));
	string send;
	bool cont = true;

	if((s = socket(PF_INET, SOCK_STREAM, 0 )) < 0) { /* create socket*/
	    printErr(ERR_SOCKET);
	}

	if ((hptr = gethostbyname(host)) == NULL) {
	    printErr(ERR_GETHOSTBYNAME);
	}

	memcpy(&sin.sin_addr, hptr->h_addr, hptr->h_length);

	/* CONNECT */

	// pokus o připojení

	if (connect (s, (struct sockaddr *)&sin, sizeof(sin) ) < 0 ){
	    printErr(ERR_CONNECT);
	}

	// přijetí zprávy - connect

	while(( n = recv(s, msg, sizeof(msg), 0)) > 0) {

		line = msg;
		line.erase(line.begin()+n, line.end());
		istringstream f(line);

		while(getline(f, line) && cont) {
			if((!isdigit(line[0]) && !isdigit(line[1]) && !isdigit(line[2])) || n < 5)
				continue;

			if(line[0] == '2' && line[1] == '2' && line[2] == '0') {
				if(line[3] == '-')
					continue;
				else
					cont = false;
			}
			else {
				printErr(ERR_NON220);
			}
		}	

		if(!cont) break;
	}

	if(n < 0)
	    printErr(ERR_READ);

	/* USER */

	// zaslání username

	if(!is_set_user)
		strcpy(user, "anonymous");

	string user_msg = "USER ";
	user_msg.append(user);
	user_msg.append("\r\n");

	if ( write(s, user_msg.c_str(), strlen(user_msg.c_str())) < 0 ) {
	    printErr(ERR_WRITE);
	}

	// přijetí zprávy - username

	cont = true;

	while(( n = recv(s, msg, sizeof(msg), 0)) > 0) {

		line = msg;
		line.erase(line.begin()+n, line.end());
		istringstream f(line);

		while(getline(f, line) && cont) {
			if((!isdigit(line[0]) && !isdigit(line[1]) && !isdigit(line[2])) || n < 5)
				continue;
			
			if(line[0] == '3' && line[1] == '3' && line[2] == '1') {
				if(line[3] == '-')
					continue;
				else
					cont = false;
			}
			else {
				printErr(ERR_NON331);
			}
		}	

		if(!cont) break;
	}

	if(n < 0)
	    printErr(ERR_READ);

	/* PASS */

	// zaslání password

	if(!is_set_pass)
		strcpy(pass, "secret");

	string pass_msg = "PASS ";
	pass_msg.append(pass);
	pass_msg.append("\r\n");

	if ( write(s, pass_msg.c_str(), strlen(pass_msg.c_str())) < 0 ) {
	    printErr(ERR_WRITE);
	}

	// přijetí zprávy - password

	cont = true;

	while(( n = recv(s, msg, sizeof(msg), 0)) > 0) {

		line = msg;
		line.erase(line.begin()+n, line.end());
		istringstream f(line);

		while(getline(f, line) && cont) {
			if((!isdigit(line[0]) && !isdigit(line[1]) && !isdigit(line[2])) || n < 5)
				continue;
			
			if(line[0] == '2' && line[1] == '3' && line[2] == '0') {
				if(line[3] == '-')
					continue;
				else
					cont = false;
			}
			else {
				printErr(ERR_NON230);
			}
		}	

		if(!cont) break;
	}

	/* PASV */

	// zaslání žádosti o pasivní mód

	if ( write(s, "PASV\r\n", 6) < 0 ) {
	    printErr(ERR_WRITE);
	}

	// přijetí zprávy pasv

	cont = true;

	while(( n = recv(s, msg, sizeof(msg), 0)) > 0) {

		line = msg;
		line.erase(line.begin()+n, line.end());
		istringstream f(line);

		while(getline(f, line) && cont) {
			if((!isdigit(line[0]) && !isdigit(line[1]) && !isdigit(line[2])) || n < 5)
				continue;
			
			if(line[0] == '2' && line[1] == '2' && line[2] == '7') {
				if(line[3] == '-')
					continue;
				else
					cont = false;
			}
			else {
				printErr(ERR_NON227);
			}
		}	

		if(!cont) break;
	}

	if(n < 0)
	    printErr(ERR_READ);

	string message = msg;
	string port1, port2;
	message.erase(message.begin()+n, message.end());
	message = message.substr(message.find("(")+1, message.find(")") - message.find("(") - 1);
	port1 = message.substr(message.find_last_of(",") + 1);
	message.erase(message.begin()+message.find_last_of(","), message.end());
	port2 = message.substr(message.find_last_of(",") + 1);

	int port_n = atoi(port2.c_str()) * 256 + atoi(port1.c_str());
	sprintf(port, "%d", port_n);

	/* CWD */

	if(is_set_path) {

		// zaslání path

		string path_msg = "CWD ";
		path_msg.append(path);
		path_msg.append("\r\n");

		if ( write(s, path_msg.c_str(), strlen(path_msg.c_str())) < 0 ) {
		    printErr(ERR_WRITE);
		}

		// přijetí zprávy - path

		cont = true;

		while(( n = recv(s, msg, sizeof(msg), 0)) > 0) {

			line = msg;
			line.erase(line.begin()+n, line.end());
			istringstream f(line);

			while(getline(f, line) && cont) {
				if((!isdigit(line[0]) || !isdigit(line[1]) || !isdigit(line[2])) || n < 5)
					continue;
				
				if(line[0] == '2' && line[1] == '5' && line[2] == '0') {
					if(line[3] == '-')
						continue;
					else
						cont = false;
				}
				else {
					printErr(ERR_NON250);
				}
			}	

			if(!cont) break;
		}

		if(n < 0)
		    printErr(ERR_READ);
	}

	struct sockaddr_in sin2;
	struct hostent *hptr2;
	int s2;

	sin2.sin_family = PF_INET;
	sin2.sin_port = htons(atoi(port));

	if((s2 = socket(PF_INET, SOCK_STREAM, 0 )) < 0) { /* create socket*/
	    printErr(ERR_SOCKET);
	}

	if ((hptr2 = gethostbyname(host)) == NULL) {
	    printErr(ERR_GETHOSTBYNAME);
	}

	memcpy(&sin2.sin_addr, hptr2->h_addr, hptr2->h_length);

	/* CONNECT */

	// pokus o připojení

	if (connect (s2, (struct sockaddr *)&sin2, sizeof(sin2) ) < 0 ){
	    printErr(ERR_CONNECT);
	}

	// zaslání list

	if ( write(s, "LIST\r\n", 6) < 0 ) {
	    printErr(ERR_WRITE);
	}

	// přijetí zprávy - list

	cont = true;

	while(( n = recv(s, msg, sizeof(msg), 0)) > 0) {

		line = msg;
		line.erase(line.begin()+n, line.end());
		istringstream f(line);

		while(getline(f, line) && cont) {
			if((!isdigit(line[0]) && !isdigit(line[1]) && !isdigit(line[2])) || n < 5)
				continue;
			
			if(line[0] == '1' && line[1] == '5' && line[2] == '0') {
				if(line[3] == '-')
					continue;
				else
					cont = false;
			}
			else {
				printErr(ERR_NON150);
			}
		}	

		if(!cont) break;
	}

	if(n < 0)
	    printErr(ERR_READ);

	while(( n = recv(s2, msg, sizeof(msg), 0)) > 0) {
		line = msg;
		line.erase(line.begin()+n, line.end());
		cout << line.substr();
	}

	if(n < 0)
	    printErr(ERR_READ);


	if (close(s2) < 0) { 
	    perror("error on close");
	    return -1;
	}

	cont = true;

	while(( n = recv(s, msg, sizeof(msg), 0)) > 0) {

		line = msg;
		line.erase(line.begin()+n, line.end());
		istringstream f(line);

		while(getline(f, line) && cont) {
			if((!isdigit(line[0]) && !isdigit(line[1]) && !isdigit(line[2])) || n < 5)
				continue;
			
			if(line[0] == '2' && line[1] == '2' && line[2] == '6') {
				if(line[3] == '-')
					continue;
				else
					cont = false;
			}
			else {
				printErr(ERR_NON226);
			}
		}	

		if(!cont) break;
	}

	if(n < 0)
	    printErr(ERR_READ);

	if (close(s) < 0) { 
	    perror("error on close");
	    return -1;
	}

	freeAll();
}

void printErr(int error_code) {
	cerr << errors[error_code] << endl;
	freeAll();
	exit(1);
}

void freeAll() {
	delete [] user;
	delete [] pass;
	delete [] host;
	delete [] port;
	delete [] path;
}

void solveParams(int argc, char ** argv) {
	if(argc != 2) {
		printErr(ERR_BADPARAMS);
	}

	user = new char[strlen(argv[1]) + 20];
	pass = new char[strlen(argv[1]) + 20];
	host = new char[strlen(argv[1]) + 20];
	port = new char[strlen(argv[1]) + 20];
	path = new char[strlen(argv[1]) + 20];

	user[0] = pass[0] = host[0] = port[0] = path[0] = '\0';

	bool will_set_port = false;

	char * tmp = new char[strlen(argv[1]) + 1];

	int j = 0; unsigned int i = 0;

	if(strncmp(argv[1], "ftp://", 6) == 0) {
		i = 6;
	}
	
	for(; i < strlen(argv[1]); i++, j++) {
		tmp[j] = argv[1][i];
		tmp[j+1] = '\0';
		if((argv[1][i] == ':' && !is_set_host) || (i == strlen(argv[1]) - 1 && !is_set_host) || (argv[1][i] == '/' && !is_set_host)) {
			strcpy(host, tmp);
			if(i == strlen(argv[1]) - 1 && argv[1][i] != '/')
				host[++j] = '\0';
			else
				host[j] = '\0';

			strcpy(tmp, "");

			is_set_host = true;

			j = -1;
		}
		else if(argv[1][i] == '@') {
			strcpy(pass, tmp);
			pass[j] = '\0';
			strcpy(user, host);
			strcpy(host, "");
			strcpy(tmp, "");

			is_set_host = false;
			is_set_user = true;
			is_set_pass = true;

			will_set_port = false;

			j = -1;
		}
		else if((argv[1][i] == '/' || i == strlen(argv[1]) - 1) && is_set_host && will_set_port && !is_set_port) {
			strcpy(port, tmp);
			if(i == strlen(argv[1]) - 1 && argv[1][i] != '/')
				port[++j] = '\0';
			else
				port[j] = '\0';

			strcpy(tmp, "");

			is_set_port = true;

			j = -1;
		}
		else if(i == strlen(argv[1]) - 1 && is_set_host) {
			strcpy(path, tmp);
			strcpy(tmp, "");

			is_set_path = true;

			j = -1;
		}

		if(argv[1][i] == ':' && is_set_host)
			will_set_port = true;
	}

	delete [] tmp;

	if(strncmp(argv[1], "ftp://", 6) != 0 && (is_set_user || is_set_pass)) {
		printErr(ERR_BADPARAMS);
	}
	if((is_set_user && is_set_pass) && ((strlen(user) == 0 && strlen(pass) != 0) || (strlen(user) != 0 && strlen(pass) == 0)  || (strlen(user) == 0 && strlen(pass) == 0)))  {
		printErr(ERR_BADPARAMS);
	}
	if(strlen(host) == 0)   {
		printErr(ERR_BADPARAMS);
	}
	if((is_set_port && strlen(port) == 0) || (is_set_path && strlen(path) == 0)) {
		printErr(ERR_BADPARAMS);
	}
	if(is_set_port) {
		for(i = 0; i < strlen(port); i++) {
			if(!isdigit(port[i])) {
				printErr(ERR_BADPARAMS);
				break;
			}
		}
	}
}
